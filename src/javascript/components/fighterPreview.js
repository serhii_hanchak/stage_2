import { createElement } from '../helpers/domHelper';

export function createFighterPreview (fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  try {
    const fighterElement = createElement({
      tagName: 'div',
      className: `fighter-preview___root ${positionClassName}`,
    });

    const fighterDetailsInfo = createElement({
      tagName: 'ul',
      className: 'details'
    })

    for (let key in fighter) {
      let data = createElement({
        tagName: 'li',
        className: 'detailsInfo'
      })
      data.append(`${key}: ${fighter[key]} `)
      fighterDetailsInfo.append(data)
    }

    let image = createFighterImage(fighter)

    fighterElement.append(image, fighterDetailsInfo)

    return fighterElement;
  } catch (err) {
    console.warn(error);
  }
}

export function createFighterImage (fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
