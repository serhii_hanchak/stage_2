import { controls, PlayerOneAttack } from '../../constants/controls';

export async function fight (firstFighter, secondFighter) {
  return new Promise((resolve) => {
    if (firstFighter.health <= 0) {
      resolve(secondFighter)
    } if (secondFighter.health <= 0) {
      resolve(firstFighter)
    }
  });
}

export function getDamage (attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender)
  if (damage < 0) {
    return 0
  } else
    return damage
}

export function getHitPower (fighter) {
  const criticalHitChance = Math.random() * 2
  const hitPower = fighter.attack * criticalHitChance

  return hitPower
}

export function getBlockPower (fighter) {
  const dodgeChance = Math.random() * 2
  const blockPower = fighter.defense * dodgeChance

  return blockPower
}
